#!/usr/bin/env python

import sys
import os
import re


### Writes the shell script file

import datetime

now = datetime.datetime.now()
date =now.strftime("%Y.%b.%d")

commandsFile = open("queue.parallel.gsa.sh", 'w')
mainDir = '/work/jpm76/magma/BS475' 


in0  =open("parallel.GSA.list", "rU")
for line_idx, line in enumerate(in0):

	fqfile=line.replace('\n', '')
	trait = line.replace('\n', '').replace('ga..', '').replace('.PVAL..2018-Feb-13.txt.genes.raw', '')
	

	sampleName = fqfile

	shellScriptName = '%s..gsa..%s.sh' % (trait,date)
	shellScript = open(shellScriptName, 'w' )


	commandsFile.write('sbatch %s \n' % (shellScriptName))
	

	shellScript.write("#!/bin/bash\n")
	shellScript.write("#\n")
	shellScript.write("#$ -S /bin/bash\n")
	shellScript.write("#SBATCH --get-user-env\n")
	shellScript.write("#SBATCH --account=biodept\n")
	shellScript.write("#SBATCH --partition=biodept")
	shellScript.write("#SBATCH --job-name=%s\n" % (sampleName))
	shellScript.write("#SBATCH --output=%s/%s..%s..gsa.out\n" % (mainDir,sampleName,date))
	shellScript.write("#SBATCH --ntasks=1\n")
	shellScript.write("#SBATCH --cpus-per-task=1\n")
	shellScript.write("#SBATCH --mail-type=ALL\n")
	shellScript.write("#SBATCH --mail-user=jpm76@duke.edu\n")
	shellScript.write("#SBATCH --workdir=%s\n" % (mainDir))
	shellScript.write("#SBATCH --mem=8G\n\n")
	
	shellScript.write("/datacommons/tmolab/julius/MAGMA/magma --gene-results %s --model fwer=100000 --set-annot /work/jpm76/magma/GSA.input.files/3k..lessGO.Boechera.BioFunction.GO.180108.txt col=2,1 self-contained --out gsa..%s..%s\n" % (sampleName,trait,date))
	
	