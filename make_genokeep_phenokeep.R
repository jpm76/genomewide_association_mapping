args = commandArgs(trailingOnly=TRUE)

suppressWarnings(suppressMessages(library(tidyverse)))
suppressWarnings(suppressMessages(library(psych)))

home.dir <- path.expand("~")
#file.path <- file.path(home.dir,"Documents", "gwas_pipeline", "master_files")


infile <- data.table::fread(args[1],
                            colClasses=c(V1="character",
                                         V2="numeric"))

#geno.selected <- file1 %>% left_join(infile)



message("----------------------")
message("Data frame check")
glimpse(infile)

message("")
message("")
message("----------------------")
message("Phenotype statistics")
describe(infile$V2)

#bs479.genotypes <- data.table::fread(paste0(file.path,"/","bs479.genotypes"))
bs479.genotypes <- data.table::fread(paste0("/dscrhome/jpm76/input.files/emmax","/","bs479.genotypes"))


geno.selected <- bs479.genotypes %>% left_join(infile, by = "V1")
message("")
message("")
message("Merged with bs479 genotypes ----------------------")
glimpse(geno.selected)
summary (geno.selected)
geno.selected <- na.omit(geno.selected)
message("")
message("")
message("Removing NA's ----------------------")
glimpse(geno.selected)
message("----------------------")
summary (geno.selected)

message("")
message("")

message("Writing pheno file...")

pheno.keep <- geno.selected %>% mutate (V3=V1) %>% select (V1,V3,V2)

write.table(pheno.keep, file = args[2], quote = F, sep = " ", row.names = F, col.names = F)
message("Done!")

geno.keep <- geno.selected %>% mutate (V3=V1) %>% select (V1, V3)

message("Writing genokeep file...")
write.table(geno.keep, file = args[3], quote = F, sep = " ", row.names = F, col.names = F)
message("Done!")