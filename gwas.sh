#!/bin/bash

# GWAS pipeline
# __author__ = Julius Mojica

# Message
echo "Script written by Julius Mojica"
echo "Last update:" `date` 
echo "This script will perform Genome-Wide Association Mapping using EmmaX."
echo "It requires a two-field tab-delimited phenotype file WITHOUT a header."
echo "Field 1 is a vector of RP genotypes."
echo "Field 2 is a vector of phenotypes."

f=$1
echo "***********************"
echo "Processing file: $f"
echo "***********************"
head $f
echo "..."
tail $f
echo "***********************"
echo "Input file ( $f ) contains `sed '1d' $f | wc -l | tr " " "\t" | cut -f6`  genotypes"


# Exclude phenotypes that do not have genotype data
sed '1d' $f > $f.headless
module load R/3.4.4
Rscript --vanilla /hpchome/tmolab/jpm76/scripts/make_genokeep_phenokeep.R $f.headless `echo $f.pheno` `echo $f.genokeep`
echo "***********************"
echo "Performing association with EmmaX..."
Emmax.sh $f
echo "EmmaX done!"
echo ""
echo ""
echo "***********************"
echo "Removing intermediate files..."

rm ./Bs475_beagle.MAF05.phy.ID.vcf* $f.headless $f\_emmax.PCAcovar.reml $f\_emmax.PCAcovar.ps $f\_emmax.PCAcovar.log $f\_col1 $f\_col1.mod