#!/bin/bash

# this shell script runs one EMMAX given one argument
# arg1 = trait name
# make sure that for each trait, you have both the <trait name>.pheno and <trait name>.genokeep


#0. subset genotype file 

#plink --bfile /dscrhome/jpm76/input.files/emmax/Bs475_beagle.MAF05.phy.ID.vcf --keep bc.ratio.genokeep --make-bed --maf 0.05 --out ./Bs475_beagle.MAF05.phy.ID.vcf_bcr

/hpchome/tmolab/bw142/plink/plink --bfile /dscrhome/jpm76/input.files/emmax/Bs475_beagle.MAF05.phy.ID.vcf --keep $1.genokeep --make-bed --maf 0.05 --out ./Bs475_beagle.MAF05.phy.ID.vcf_$1

#1. create the genotype file

#plink --bfile ./Bs475_beagle.MAF05.phy.ID.vcf_bcr --keep bc.ratio.genokeep --recode12 --output-missing-genotype 0 --transpose --out ./Bs475_beagle.MAF05.phy.ID.vcf_bcr

/hpchome/tmolab/bw142/plink/plink --bfile ./Bs475_beagle.MAF05.phy.ID.vcf_$1 --keep $1.genokeep --recode12 --output-missing-genotype 0 --transpose --out ./Bs475_beagle.MAF05.phy.ID.vcf_$1

#2. create kinship matrix 

#emmax-kin -v -h -d 10 Bs475_beagle.MAF05.phy.ID.vcf_bcr

/opt/apps/rhel7/emmax/emmax-beta-07Mar2010/emmax-kin -v -h -d 10 Bs475_beagle.MAF05.phy.ID.vcf_$1


#3. create PCA of the genotype file


#plink --bfile Bs475_beagle.MAF05.phy.ID.vcf_bcr --pca 4 --out Bs475_beagle.MAF05.phy.ID.vcf_bcr

/hpchome/tmolab/bw142/plink/plink --bfile Bs475_beagle.MAF05.phy.ID.vcf_$1 --pca 4 --out Bs475_beagle.MAF05.phy.ID.vcf_$1


#4. create a covariate file from the PCA

base="Bs475_beagle.MAF05.phy.ID.vcf_"$1
ext=".eigenvec"
var="$base$ext"
lines=$(wc -l < "$var")
for (( c=1; c<="$lines"; c++)) ; do echo "1" ; done > intercept_$1
cut -d' ' -f 1,2  $var > col12_$1
cut -d' ' -f3- $var > lastcols_$1
paste -d' ' col12_$1 intercept_$1 lastcols_$1 > $base".covariate"
rm intercept_$1 col12_$1 lastcols_$1


#5. run emmax

#emmax -v -d 10 -t Bs475_beagle.MAF05.phy.ID.vcf_bcr -p bcratio.pheno -k Bs475_beagle.MAF05.phy.ID.vcf_bcr.hBN.kinf -c Bs475_beagle.MAF05.phy.ID.vcf_bcr.covariate -o bcr_emmax

/opt/apps/rhel7/emmax/emmax-beta-07Mar2010/emmax -v -d 10 -t Bs475_beagle.MAF05.phy.ID.vcf_$1 -p $1.pheno -k Bs475_beagle.MAF05.phy.ID.vcf_$1.hBN.kinf -c Bs475_beagle.MAF05.phy.ID.vcf_$1.covariate -o $1_emmax.PCAcovar

#emmax -v -d 10 -t Bs475_beagle.MAF05.phy.ID.vcf.emmax -p total.pheno -k Bs475_beagle.MAF05.phy.ID.vcf.emmax.hBN.kinf -c ft.covar -o total.emmax.covar

#emmax -v -d 10 -t Bs475_beagle.MAF05.phy.ID.vcf.emmax -p total.pheno -k Bs475_beagle.MAF05.phy.ID.vcf.emmax.hBN.kinf -c ft.covar -o total.emmax.covar



#6. Create a file for Manhattanplot


cut -f1 -d$'\t'  $1_emmax.PCAcovar.ps > $1_col1


tr "." "\t" < $1_col1 > $1_col1.mod

paste $1_emmax.PCAcovar.ps $1_col1.mod > $1_emmax.PCAcovar.ps.mod # use this for manhattan plot in R


