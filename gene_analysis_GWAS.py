#!/usr/bin/env python

import sys
import os


### Writes the shell script file

import datetime

now = datetime.datetime.now()
date =now.strftime("%Y-%b-%d")

commandsFile = open("cyc.queue.parallel.ga.sh", 'w')
mainDir = '/work/jpm76/magma/BS475/fitness.herbivory' 


in0  =open("GA.list", "rU")
for line_idx, line in enumerate(in0):

	fqfile=line.replace('\n', '')
	cols = line.replace('\n', '').split('_')
	trait = line.replace('\n', '').replace('.PVAL', '')
	

	sampleName = fqfile

	shellScriptName = 'cyc.%s..%s.sh' % (trait,date)
	shellScript = open(shellScriptName, 'w' )


	commandsFile.write('sbatch %s \n' % (shellScriptName))
	
	shellScript.write("#!/bin/bash\n")
	shellScript.write("#\n")
	shellScript.write("#$ -S /bin/bash\n")
	shellScript.write("#SBATCH --get-user-env\n")
	shellScript.write("#SBATCH --partition=tmolab\n")
	shellScript.write("#SBATCH --job-name=%s..cyc\n" % (trait))
	shellScript.write("#SBATCH --error=%s..%s..ga..cyc.err\n" % (trait,date))
	shellScript.write("#SBATCH --output=%s..%s..ga..cyc.out\n" % (trait,date))
	shellScript.write("#SBATCH --ntasks=1\n")
	shellScript.write("#SBATCH --cpus-per-task=1\n")
	shellScript.write("#SBATCH --mail-type=ALL\n")
	shellScript.write("#SBATCH --mail-user=jpm76@duke.edu\n")
	shellScript.write("#SBATCH --workdir=%s\n" % (mainDir))
	shellScript.write("#SBATCH --mem=8G\n\n")
	
	shellScript.write("magma --bfile /dscrhome/jpm76/input.files/magma/Bs475_beagle.MAF05.phy.ID.vcf --pval %s N=479 --gene-annot /dscrhome/jpm76/input.files/magma/cyc.1k1k.genes.annot --gene-model multi=snp-wise --out \"ga..cyc..%s..%s.txt\"\n" % (sampleName,trait,date))
	
	
	
